# Event Ticket

This module integrates the [Event](https://www.drupal.org/project/event],
[Event Registration](https://www.drupal.org/project/event_registration], and
[Commerce](https://www.drupal.org/project/commerce) modules, to provide a simple
experiance for purchasing event tickets.
